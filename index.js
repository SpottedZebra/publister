#!/usr/bin/env node
"use strict";

var util = require("util");
var path = require("path");
var http = require("http");
var fs = require("fs");

var express = require("express");
var app = express();
var sqlite3 = require("sqlite3");

const DB_PATH = path.join(__dirname, "my.db");
const DB_SQL_PATH = path.join(__dirname, "mydb.sql");
const WEB_PATH = path.join(__dirname, "web");
const HTTP_PORT = 8080;

var myDB = new sqlite3.Database(DB_PATH);
var SQL3 = {
  run(...args) {
    return new Promise(function c(resolve, reject) {
      myDB.run(...args, function onResult(err) {
        if (err) reject(err);
        else resolve(this);
      });
    });
  },
  get: util.promisify(myDB.get.bind(myDB)),
  all: util.promisify(myDB.all.bind(myDB)),
  exec: util.promisify(myDB.exec.bind(myDB))
};

var httpserv = http.createServer(app);

main();

async function main() {
  defineRoutes(app);

  var initSQL = fs.readFileSync(DB_SQL_PATH, "utf-8");
  await SQL3.exec(initSQL);

  httpserv.listen(HTTP_PORT);
  console.log(`Listening on http://localhost:${HTTP_PORT}...`);
}

async function defineRoutes(app) {
  app.get(/\/get-pubs\b/, async function getRecords(req, res) {
    let records = (await getAllEntries()) || [];
    res.setHeader("Content-Type", "application/json");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.writeHead(200);
    res.end(JSON.stringify(records));
  });

  app.get(/post/, async function(req, res) {
    const { name, location, desc } = req.query;
    const result = await insertOrLookup(name, location, desc);
    if (result.result.changes > 0) {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Inserted into db");
    } else if (result) {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Already exists");
    } else {
      console.log(error);
    }
  });

  app.use(function rewriter(req, res, next) {
    if (/^\/(?:index\/?)?(?:[?#].*$)?$/.test(req.url)) {
      res.setHeader("Content-Type", "text/plain");
      res.end("where index should be");
      req.url = "/index.html";
    } else if (/^\/js\/.+$/.test(req.url)) {
      next();
      return;
    } else if (/^\/(?:[\w\d]+)(?:[\/?#].*$)?$/.test(req.url)) {
      let [, basename] = req.url.match(/^\/([\w\d]+)(?:[\/?#].*$)?$/);
      req.url = `${basename}.html`;
    }
    next();
  });

  var fileServer = express.static(WEB_PATH, {
    maxAge: 100,
    setHeaders(res) {
      res.setHeader("Server", "List backend");
    }
  });

  app.use(fileServer);

  app.get(/\.html$/, function friendly404(req, res, next) {
    req.url = "/404.html";
    fileServer(req, res, next);
  });
}

// Helper functions
const resultTypes = {
  INSERTED: "inserted",
  EXISTING: "existing"
};

async function getAllEntries() {
  var result = await SQL3.all(
    `
      SELECT
        name, location, desc
      FROM
        Publist
    `
  );

  if (result && result.length > 0) {
    return result;
  }
  return false;
}

async function insertOrLookup(name, location, desc) {
  var result = await SQL3.get(
    `
			SELECT
				id
			FROM
				Publist
			WHERE
        name = ?
        AND 
        location = ?
		`,
    name,
    location
  );
  if (result && result.id) {
    return { result: result };
  } else {
    result = await SQL3.run(
      `
			INSERT INTO
				Publist
			(name, location, desc)
			VALUES
				(?, ?, ?)
			`,
      name,
      location,
      desc
    ).catch(error => console.warn(error));
  }
  if (result && result.lastID) {
    return { result: result, resultMessage: resultTypes.INSERTED };
  }
  return false;
}

async function removeEntry(name, location) {
  var result = SQL3.run(
    `
      DELETE FROM
        Publist
      WHERE
        name = ?
      AND
        location = ?
    `,
    name,
    location
  );
  if (result && result.changes > 0) {
    return result;
  }
  return false;
}
